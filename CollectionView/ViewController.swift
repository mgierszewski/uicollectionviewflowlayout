// 
// Copyright © 2020 Future Mind Sp. z o. o.
//

import UIKit

class Cell: UICollectionViewCell {
    @IBOutlet var titleLabel: UILabel!

    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        let size = systemLayoutSizeFitting(layoutAttributes.frame.size,
                                           withHorizontalFittingPriority: .required,
                                           verticalFittingPriority: .fittingSizeLevel)

        layoutAttributes.frame = CGRect(origin: layoutAttributes.frame.origin,
                                     size: size)

        return layoutAttributes
    }
}

class ViewController: UICollectionViewController {
    let labels = [
        "lorem ipsum dolor sit amet",
        "consectetur adipiscing elit",
        "pellentesque eget volutpat urna",
        "praesent vulputate viverra nibh",
        "vulputate ex tristique sed",
        "aliquam erat volutpat",
        "nulla venenatis felis",
        "aendrerit massa aliquet venenatis"
    ]

    override func viewDidLoad() {
        super.viewDidLoad()

        // setting estimated size to .zero -> cell's fixed size (returned in collectionView:collectionViewLayout:sizeForItemAt:)
        // (collectionViewLayout as! UICollectionViewFlowLayout).estimatedItemSize = .zero

        // setting estimated size to non-zero -> cell will self-size
        // to the bounds returned in collectionView:collectionViewLayout:sizeForItemAt:
        // which can be adjusted by cell's preferredLayoutAttributesFitting method
        (collectionViewLayout as! UICollectionViewFlowLayout).estimatedItemSize = CGSize(width: -1.0, height: -1.0)
    }

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! Cell
        cell.titleLabel.text = labels[indexPath.row % labels.count]
        cell.backgroundColor = .red
        
        return cell
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let bounds = collectionView.bounds.inset(by: flowLayout.sectionInset)
        let spacing = flowLayout.minimumInteritemSpacing

        if indexPath.row < 3 {
            // one column
            return CGSize(width: bounds.width, height: 40)

        } else if indexPath.row < 7 {
            // two columns
            let itemWidth = (bounds.width - spacing)/2.0
            return CGSize(width: itemWidth, height: 40)
        }

        // three columns
        let itemWidth = (bounds.width - 2.0 * spacing)/3.0
        return CGSize(width: itemWidth, height: 40)
    }
}
